## Kabaret background session example

This extension is meant to illustrate how to access the flow and give feedbacks using a background session. It provides an action which runs the session in a subprocess updating the action's progress bar. The action is registered as the first child in the project root.

![image](data/progress_bar.gif)