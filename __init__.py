import os
import re
import sys
import subprocess
from kabaret import flow
from kabaret.flow.object import _Manager


class LaunchWorkerSession(flow.Action):
    '''
    Runs the `worker_session.py` script in a subprocess.
    '''
    progress = flow.Param(0).ui(editor='percent', editable=False)
    running = flow.BoolParam(False).ui(hidden=True)

    def needs_dialog(self):
        if not self.running.get():
            self.progress.revert_to_default()
        return True

    def get_buttons(self):
        return ['Launch !']

    def run(self, button):
        # Pass on data required by render session through the environment
        env = os.environ
        env['LAUNCH_ACTION_OID'] = self.oid()
        
        self.running.set(True)
        subprocess.Popen([
            sys.executable,
            os.path.join(os.path.dirname(__file__), 'worker_session.py')],
            env=env)
        
        return self.get_result(close=False)


class LaunchObject(flow.Object):
    MANAGER_TYPE = _Manager
    launch_action = flow.Child(LaunchWorkerSession).ui(label='Launch')


def launch_worker_action(parent):
    # Register the action in the project root
    if re.fullmatch(r'/[^/]+', parent.oid()) is not None:
        r = flow.Child(LaunchObject)
        r.name = 'launch_object'
        r.index = 0
        return r


def install_extensions(session): 
    return {
        "test_progress_bar": [
            launch_worker_action,
        ],
    }
