# This script connects to the current Kabaret Redis cluster
# and updates the progress bar of the action which launched it.

import os
import time
from kabaret.app.session import KabaretSession


class WorkerSession(KabaretSession):

    def __init__(self):
        super(WorkerSession, self).__init__('WorkerSession')
        self.cmds.Cluster.connect_from_env()
    
    def launch(self):
        action_oid = os.environ.get('LAUNCH_ACTION_OID')
        if action_oid is None:
            return
        
        # Action retrieved directly in the session for convenience.
        # It should have a dedicated actor to access the flow.
        # See kabaret.jobs actor for example:
        # https://gitlab.com/kabaretstudio/kabaret.jobs/-/blob/master/python/kabaret/jobs/jobs_actor.py?ref_type=heads#L890
        try:
            action = self.get_actor('Flow').get_object(action_oid)
        except Exception as e:
            self.log_error(f'No action with oid {action_oid}')
            self.log_error(str(e))
            return
        
        for i in range(101):
            action.progress.set(i)
            time.sleep(.01)
        action.running.set(False)


if __name__ == '__main__':
    s = WorkerSession()
    s.launch()
